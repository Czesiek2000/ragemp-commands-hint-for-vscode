const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs');
const file = fs.createWriteStream('save.js');

const className = 'Cursor.visible';
const functionName = 'id';
const urlTemplate = `https://wiki.rage.mp/index.php?title=${className}::${functionName}`;


request('https://wiki.rage.mp/index.php?title=Cursor.position', (error, response, html) => {
    if (!error && response.statusCode == 200) {
        const $ = cheerio.load(html);

        const heading = $('#firstHeading').text();
        const code = $('.mw-parser-output').find('pre').eq(1).text();
        const exampleHeading = $('h2').eq(2).text();
        const example = $('.mw-parser-output').find('pre').eq(0).text();
        // const example2 = $('.mw-parser-output').find('pre').eq(3).text();
        const description = $('.mw-parser-output').find('p').eq(1).text();
        const ul = $('ul').eq(2).text();
        const subHeading = $('.mw-parser-output p').text();

        file.write(`${heading} // Heading \n`);
        file.write(`// Code \n${code} \n`);
        file.write(`// example \n${example}  \n`);
        // file.write(`// \n${example2}  \n`);
        file.write(`// ${exampleHeading}: \n`);
        file.write(`${ul} \n`);
        file.write(`${description} \n`);
        file.write(`// subheading: \n${subHeading} \n`);
        
        
    }
})

